﻿using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        public List<Vehicle> Vehicles { get; set; }
        public decimal Balance { get; set; }
    }
}