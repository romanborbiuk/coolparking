﻿using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {      
        public static readonly int InitialBalanceOfParking;
        public static readonly int ParkingSpace;
        public static readonly int PaymentWriteOffPeriod;
        public static readonly int WritingToTheLogPeriod;
        public static readonly decimal Fine;
        public static readonly Dictionary<VehicleType, double> Prices;

        static Settings()
        {
            InitialBalanceOfParking = 0;
            ParkingSpace = 10;            
            PaymentWriteOffPeriod = 5;
            WritingToTheLogPeriod = 60;
            Fine = 2.5m;
            
            Prices = new Dictionary<VehicleType, double>()
            {
                { VehicleType.PassengerCar, 2 },
                { VehicleType.Truck, 5 },
                { VehicleType.Bus, 3.5 },
                { VehicleType.Motorcycle, 1 }
            };
        }       
    }
}