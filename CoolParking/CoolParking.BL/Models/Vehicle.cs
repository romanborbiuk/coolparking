﻿using System;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            Id = GenerateRandomRegistrationPlateNumber(id);
            VehicleType = vehicleType;
            Balance = balance;
        }

        // TODO: DRY principle and refactoring is required.
        public static string GenerateRandomRegistrationPlateNumber(string plateNumber)
        {
            Random r = new Random();
            for (int i = 0; i < 2; i++) plateNumber += (char)r.Next(65, 90);
            plateNumber += "-";
            for (int i = 0; i < 4; i++) plateNumber += r.Next(0, 9).ToString();
            plateNumber += "-";
            for (int i = 0; i < 2; i++) plateNumber += (char)r.Next(65, 90);
            return plateNumber;
        }
    }
}