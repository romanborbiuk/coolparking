﻿using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public DateTime TransactionTime { get; set; }
        public int VehicleId { get; set; }
        public decimal Payment { get; set; }

        public override string ToString()
        {
            return "TransactionTime: " + TransactionTime + " Vehicle Id: " + VehicleId + " Payment: " + Payment;
        }
    }
}